![image](https://www.supportsages.com/sscontent/uploads/2019/09/firewalled.jpg)

# Como abrir e fechar portas

O Firewalld é uma ferramenta poderosa e simples de usar para gerenciar um firewall no servidor RHEL 8 /CentOS 8 ou na estação de trabalho GNOME . O Firewalld permite gerenciar portas abertas ou fechadas usando serviços predefinidos, bem como abrir e fechar portas personalizadas do usuário.

Neste procedimento você aprenderá:

- Como abrir e fechar portas no RHEL 8 / CentOS 8 com Firewalld.
- Como abrir e fechar serviços no RHEL 8 / CentOS 8.
- Como recarregar a configuração do firewall.
- Como listar portas ou serviços abertos.
- Abrindo e fechando portas de firewall no RHEL 8 / CentOS 8 usando o comando firewall-cmd.
- Abrindo e fechando portas de firewall no RHEL 8 / CentOS 8 usando firewall-cmdo comando.


Primeiro, verifique as portas já abertas ou o serviço systemd ativado . Pode ser que a porta que você está tentando abrir já esteja disponível, caso em que não há nada a fazer:
```shell
firewall-cmd --list-all
```

Verifique se o serviço com o qual você está tentando configurar seu firewall está disponível como um recurso pré-configurado. O comando abaixo listará todos os serviços prontos para uso:
```shell
firewall-cmd --get-services
```

Obtenha uma lista de zonas nas quais você deseja que a porta seja aberta:
```shell
firewall-cmd --get-zones
```

Na maioria dos casos, você está interessado na publiczona que é a zona de firewall padrão para todas as operações sem fornecer explicitamente o nome da zona como um argumento para o firewall-cmdcomando.

Porta aberta ou serviço.
Se o serviço com o qual você deseja configurar seu firewall estiver disponível como um recurso pré-configurado conforme recuperado emPasso 2use seu nome para abrir a porta.

Por exemplo, vamos abrir a porta de serviço HTTP para a zona public:
```shell
firewall-cmd --zone=public --permanent --add-service=http
```
Caso a porta que deseja abrir não faça parte dos serviços pré-configurados utilize a opção **--add-port**. Por exemplo, vamos abrir a porta TCP 8080 para a zona public:
```shell
firewall-cmd --zone=public --permanent --add-port 8080/tcp
```

Recarregue as configurações do firewall. Depois de abrir a porta ou os serviços, certifique-se de recarregar o firewall:
```shell
firewall-cmd --reload
```

Confirme se a porta ou serviço foi aberto com sucesso:
```shell
firewall-cmd --list-all
```

# Como fechar portas
Abrir portas no sistema RHEL 8 é um procedimento bastante simples. Aqui está como vai passo a passo:

Primeiro verifique se há portas ou serviços já abertos . Anote a zona, o protocolo, bem como a porta ou serviço que deseja fechar:
```shell
firewall-cmd --list-all
```

Fechar porta ou serviço. O comando abaixo fechará o httpserviço na publiczona:
```shell
firewall-cmd --zone=public --permanent --remove-service http
```

Caso deseje fechar uma porta específica, use a opção **--remove-port**. Por exemplo vamos fechar a porta TCP 8080:
```shell
firewall-cmd --zone=public --permanent --remove-port 8080
```

Recarregue as configurações do firewall:
```shell
firewall-cmd --reload
```

Confirme se a porta ou serviço foi fechado com sucesso:
```shell
firewall-cmd --list-all
```
