

Adicione esta configuração no arquivo abaixo:
`vim /etc/openfortivpn/config`

```conf
host=server.vpn
port=porta
username=seuusuario
password=suasenha
```

Conecte-se a VPN:
```bash
openfortivpn
```

Irá apresentar um erro na conexão, mas é esperado, precisamos pegar o hash para o trusted-cert.
se parece mais ou menos assim:
```
ERROR:  Gateway certificate validation failed, and the certificate digest is not in the local whitelist. If you trust it, rerun with:
ERROR:      --trusted-cert 9154b30de37d7967ffe956d7993291c85390cb76c506feba708e981e0ca8806f
ERROR:  or add this line to your configuration file:
ERROR:      trusted-cert = 9154b30de37d7967ffe956d7993291c85390cb76c506feba708e981e0ca8806f
```

Copie então esta linha para o arquivo
```conf
host=server.vpn
port=porta
username=seuusuario
password=suasenha
trusted-cert=9154b30de37d7967ffe956d7993291c85390cb76c506feba708e981e0ca8806f
```

Agora a conexão será bem-sucedida.
