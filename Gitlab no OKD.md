# Introdução

![](https://about.gitlab.com/images/press/logo/png/gitlab-logo-100.png)
Sim, senhores(as), minha primeira jornada no OKD é a instalação e persistência de dados do Gitlab.
Demorei muito mas encontrei a solução, de forma simples e eficaz.

## 1. Crie um projeto
Faça login com admin no OKD, se caso houver problema com senha, possívelmente você precisa estar como root no servidor, em seguida crie os PersistenVolumes:
```shell
oc login -u system:admin
```

Vamos criar um projeto, equivalente a um namespace para isolar nosso deploy de outros.
```shell
oc new-project gitlab
```

## Autorização
Haverá um problema de permissão, uma vez que este container executa como root. Para solucionar, vamos conceder permissão para usar o usuário root neste container.
```shell
oc adm policy add-scc-to-user anyuid -z default -n gitlab
```

Verifique se a conta de serviço padrão do projeto gitlab foi adicionada.
```shell
oc edit scc anyuid
```

A sintaxe esperada dentrod o YML será mais ou menos da seguinte forma:
```yml
users:
- system:serviceaccount:gitlab:default
```

## Criando volumes

Crie este arquivo .yml para montagem dos PersistentVolumes:
`vim arquivo.yml`

```yml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0001
spec:
  capacity:
    storage: 5Gi
  accessModes:
  - ReadWriteOnce
  hostPath:
    path: /srv/openshift-gitlab/pv0001
  persistentVolumeReclaimPolicy: Recycle
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0002
spec:
  capacity:
    storage: 5Gi
  accessModes:
  - ReadWriteOnce
  hostPath:
    path: /srv/openshift-gitlab/pv0002
  persistentVolumeReclaimPolicy: Recycle
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0003
spec:
  capacity:
    storage: 5Gi
  accessModes:
  - ReadWriteOnce
  hostPath:
    path: /srv/openshift-gitlab/pv0003
  persistentVolumeReclaimPolicy: Recycle
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0004
spec:
  capacity:
    storage: 5Gi
  accessModes:
  - ReadWriteOnce
  hostPath:
    path: /srv/openshift-gitlab/pv0004
  persistentVolumeReclaimPolicy: Recycle
```

## Crie o deploy

Vamos efetuar o deploy do Gitlab, no caso abaixo, vamos executar a versão específica, substitua conforme sua necessidade.
```shell
oc new-app gitlab/gitlab-ce:14.3.4-ce.0
```
Veja o pod
```shell
oc get pods
```

Caso desejar ver o log do pod
```shell
oc logs -f NOMEDOPOD
```



## 4. Rebuild

refaça o deploy do gitlab para identificar a permissão de root:
```shell
oc deploy gitlab-ce:14.3.4-ce.0
```




## 6 Trocando a senha

No Gitlab quando sobe a primeira vez é adicionado uma senha aleatória. Acesse o pod pelo terminal e execute o seguinte comando para coletar a senha:
```shell
grep 'Password:' /etc/gitlab/initial_root_password
```
Após encontra-la, acesse o Gitlab e troque a senha.

## 7. Crie a rota
No próprio OKD você poderá criar a rota na porta 80.



## Fonte
- http://wiki.rockplace.co.kr/display/OP/6.+GitLab
- https://docs.gitlab.com/ee/install/docker.html
