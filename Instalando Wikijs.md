# Red Hat


## Ambiente homologado
- Red Hat 8
- Node JS 16.x
- PostgreSQL 13.x
- Wiki JS 2.5.x


## Instalando Postgres

Devemos habilitar o módulo mais recente do Postrgresql no sistema operacional.
Vamos procurar o módulo com o comando abaixo:
```shell
dnf module list
```

O a versão 13, no momento desta implantação é a mais recente. Vamos instalar este módulo:
```shell
dnf module install postgresql:13 nodejs:16 -y
```

Instalando o Postgresql
```shell
dnf install -y postgresql postgresql-contrib npm
```

Iniciar o serviço, habilitar para Inicializar junto com sistema Operacional e ver o status:
```shell
postgresql-setup --initd && \
systemctl start postgresql && \
systemctl enable postgresql && \
systemctl status postgresql

```

## Criando usuário e database
Deve ser acessado com o usuário Postgres no Shell do sistema, em seguida, acessar a Home do usuário.
```shell
su postgres && \
cd ~/
```

Criando a base de dados com o seguinte comando:
```sql
psql
CREATE DATABASE wiki;

create user wikijs with encrypted password 'wikijsrocks';
grant all privileges on database wiki to wikijs;

GRANT CONNECT ON DATABASE wiki TO wikijs;
ALTER USER  wikijs CREATEDB;
\q
```

Saindo da sessão do usuário **postgres** para o **root**
```
exit
```

## Permissão de acesso a database

Para ter acesso ao Banco de Dados, deve ser incluído esta permissão no arquivo abaixo:
`vim /var/lib/pgsql/data/postgresql.conf`

Inserir o seguinte parâmetro
```conf
listen_addresses = '*'
```

Incluir este parãmetro no final do aquivo: 
`vim /var/lib/pgsql/data/pg_hba.conf`
```conf
host    all             all             0.0.0.0/0               md5
```

Após isto, deve ser reiniciado o Banco de dados.
```shell
systemctl restart postgresql
```

## Implantando a Wiki

Abaixo, será efetuado a criação de um diretório, acessado este diretório, baixado e extraído a Wiki JS. 
```shell
mkdir /opt/wikijs && cd /opt/wikijs && \
wget https://github.com/Requarks/wiki/releases/latest/download/wiki-js.tar.gz; tar -xf *.tar.gz
```

## Liberar porta de acesso

O Firewall do sistema operacional necessita de liberar a porta que será utilizada no Sistema Operacionl, deve ser executado a sequência de comandos abaixo:
```shell
firewall-cmd --zone="public" --add-port=80/tcp --permanent && \
firewall-cmd --reload
```

## Configurando a Wiki JS
Deve ser alterado neste arquivo:
- A comunicação do banco de dados
- A porta 3000 para 80
- Caso o server estiver offline, deve ser alterado a opção **offline: true**

`vim /etc/systemd/system/wikijs.service`



## Inicialização do serviço

Pode ser executado um teste no serviço executando o comando abaixo, dentro do diretório **/opt/wikijs**.
```shell
node server
```

Se todas as etapas acima forem confirmadas com sucesso na execução deste comando, deve ser criado um arquivo para manipulação do serviço no SystemD do Sistema Operacional.

Deve ser criado o arquivo `/etc/systemd/system/wikijs.service`
Inclua o seguinte conteúdo:
```conf
[Unit]
Description=Wiki
After=network.target

[Service]
Type=simple
ExecStart=/usr/bin/node server
Restart=always

User=root
Environment=NODE_ENV=production
WorkingDirectory=/opt/wikijs

[Install]
WantedBy=multi-user.target
```

Preferêncialmente, execute cada linha separadamente para confirmar êxito. As linhas abaixo farão:
- Reiniciar o daemon
- Iniciar o serviço
- Habilitar na inicialização do sistema operacional
- Verificar o status do serviço

```
systemctl daemon-reload
systemctl start wikijs
systemctl enable wikijs
systemctl status wikijs
```

## Instalando idioma pt_BR offline

Criar o diretório de Downloads offline
```
mkdir -p /opt/wikijs/data/sideloads
```

Incluir o conteúdo do repositório de idiomas [https://github.com/Requarks/wiki-localization](https://github.com/Requarks/wiki-localization) no dirtório sideloads.

> Esta ação só será bem-sucedida se caso a etapa de **Configurando a Wiki JS** estiver em modo offline.
{.is-warning}


Fonte:
- https://serverspace.io/support/help/install-wikijs-on-ubuntu-20-04/
- https://docs.requarks.io/install/sideload

# Docker Compose

Apenas uma etapa é necessária, a criação deste arquivo e a execução com **docker-compose up -d**

```yaml
version: "3"
services:

  db:
    image: postgres:11-alpine
    environment:
      POSTGRES_DB: wiki
      POSTGRES_PASSWORD: wikijsrocks
      POSTGRES_USER: wikijs
    logging:
      driver: "none"
    restart: unless-stopped
    volumes:
      - db-data:/var/lib/postgresql/data

  wiki:
    image: ghcr.io/requarks/wiki:2
    depends_on:
      - db
    environment:
      DB_TYPE: postgres
      DB_HOST: db
      DB_PORT: 5432
      DB_USER: wikijs
      DB_PASS: wikijsrocks
      DB_NAME: wiki
    restart: unless-stopped
    ports:
      - "80:3000"

volumes:
  db-data:
```

# Kubernetes e OKD

### Deploy do Banco de Dados

Criar o arquivo `vim db-deployment.yaml`
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    kompose.cmd: kompose convert
    kompose.version: 1.27.0 (b0ed6a2c9)
  creationTimestamp: null
  labels:
    io.kompose.service: db
  name: db
spec:
  replicas: 1
  selector:
    matchLabels:
      io.kompose.service: db
  strategy: {}
  template:
    metadata:
      annotations:
        kompose.cmd: kompose convert
        kompose.version: 1.27.0 (b0ed6a2c9)
      creationTimestamp: null
      labels:
        io.kompose.service: db
    spec:
      containers:
        - env:
            - name: POSTGRES_DB
              value: wiki
            - name: POSTGRES_PASSWORD
              value: wikijsrocks
            - name: POSTGRES_USER
              value: wikijs
          image: postgres:11-alpine
          name: db
          resources: {}
      restartPolicy: Always
status: {}
```

## Deploy da Wiki

Criar o arquivo de `wiki-deployment.yaml` 
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    kompose.cmd: kompose convert
    kompose.version: 1.27.0 (b0ed6a2c9)
  creationTimestamp: null
  labels:
    io.kompose.service: wiki
  name: wiki
spec:
  replicas: 1
  selector:
    matchLabels:
      io.kompose.service: wiki
  strategy: {}
  template:
    metadata:
      annotations:
        kompose.cmd: kompose convert
        kompose.version: 1.27.0 (b0ed6a2c9)
      creationTimestamp: null
      labels:
        io.kompose.service: wiki
    spec:
      containers:
        - env:
            - name: DB_HOST
              value: db
            - name: DB_NAME
              value: wiki
            - name: DB_PASS
              value: wikijsrocks
            - name: DB_PORT
              value: "5432"
            - name: DB_TYPE
              value: postgres
            - name: DB_USER
              value: wikijs
          image: ghcr.io/requarks/wiki:2
          name: wiki
          ports:
            - containerPort: 3000
          resources: {}
      restartPolicy: Always
status: {}
```

## Serviço da Wiki

Deve ser criado o arquivo `wiki-service.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    kompose.cmd: kompose convert
    kompose.version: 1.27.0 (b0ed6a2c9)
  creationTimestamp: null
  labels:
    io.kompose.service: wiki
  name: wiki
spec:
  ports:
    - name: "80"
      port: 80
      targetPort: 3000
  selector:
    io.kompose.service: wiki
status:
  loadBalancer: {}
````

## Implantação

Após a criação de todos este arquivos, deve ser executado o comando **oc create -f nomedoarquivo.yml**

Quando todos os deploies forem concluídos, deve ser criado uma rota para a Wiki JS.

# Podman

Pode Ser utilizado com Red Hat e derivados.

## Instalando o ambiente
```
yum install -y podman-docker podman-compose
```

## Persistência de dados
```
mkdir -p  /opt/wiki/postgresql ; chown 70:70 /opt/wiki/postgresql
```

## Criando o POD
Para publicar porta e ligação entre containers
```
podman pod create -n wiki -p 3000:3000
```

## Container Postgresql no POD wiki
```shell
podman run -dit \
--name db \
-e POSTGRES_DB=wiki \
-e POSTGRES_PASSWORD=wikijsrocks \
-e POSTGRES_USER=wikijs \
--pod wiki \
-v /opt/wiki/postgresql:/var/lib/postgresql/data:z \
docker.io/library/postgres:11-alpine
```

## Container Wiki no POD wiki
```shell
podman run -dit \
--name wikijs \
-e DB_TYPE=postgres \
-e DB_HOST=db \
-e DB_PORT=5432 \
-e DB_USER=wikijs \
-e DB_PASS=wikijsrocks \
-e DB_NAME=wiki \
--pod wiki \
ghcr.io/requarks/wiki:2
```

## Firewall
Incluir a porta 3000 no firewall do sistema operacional
```shell
firewall-cmd --zone=public --permanent --add-port 3000/tcp && \
firewall-cmd --reload
```
