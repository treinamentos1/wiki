```
apt install -y gcc vim curl wget g++ libcairo2-dev libjpeg-turbo8-dev libpng-dev libtool-bin libossp-uuid-dev libavcodec-dev  libavformat-dev libavutil-dev libswscale-dev build-essential libpango1.0-dev libssh2-1-dev libvncserver-dev libtelnet-dev libpulse-dev libssl-dev libvorbis-dev libwebp-dev libwebsockets-dev
```

```
cd ~/
wget https://archive.apache.org/dist/guacamole/1.5.0/source/guacamole-server-1.5.0.tar.gz
tar xzf ~/guacamole-server-*.tar.gz
cd ~/guacamole-server-*/
./configure --with-init-dir=/etc/init.d
```
```
sudo make
sudo make install
sudo ldconfig
```
