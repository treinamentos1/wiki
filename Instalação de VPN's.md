## GlobalProtect
Para instalar esta VPN, recomento a forma mais simples em modo GUI.
Instale esta dependência:
```
sudo apt-get -y install libqt5webkit5
```

Baixe este pacote chamado **PanGPLinux-5.3.0-c32.tgz** na URL [https://vpn.wisc.edu/clients/](https://vpn.wisc.edu/clients/)

Extraia o pacote `GlobalProtect_UI_deb-5.1.0.0-62.deb`

Em seguida, instale-o com o comando abaixo:
```
sudo dpkg -i GlobalProtect_UI_deb-5.1.0.0-62.deb
```

O botão de conexão aparecerá no seu painel de tarefas.

Fonte:
- https://docs.paloaltonetworks.com/globalprotect/5-1/globalprotect-app-user-guide/globalprotect-app-for-linux/download-and-install-the-globalprotect-app-for-linux
- https://wiki.albany.edu/pages/releaseview.action?pageId=82351001
