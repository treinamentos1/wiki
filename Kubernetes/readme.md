# Aula 1.3

### POD

Criar pod
```
kubectl run pod1 --image=nginx
```
Listar pod
```
kubectl get pods
```
Acessar o Pod
```
kubectl exec -it pod1 -- bash
```
Expondo o pod pela porta para o acesso
```
kubectl port-forward --address IP-NODE pod1 8081:80
```
Remover pod
```
kubectl delete pod pod1
```

### Deploy
Criando um deploy
```
kubectl create deploy app1 --image nginx --replicas=2
```
Listando deploy
```
kubectl get deploy
```
Removendo Deploy
```
kubectl delete deploy app1
```
Criar deploy do serviço através de um YAML automático
```
kubectl create deploy app1 --image=nginx --dry-run=client -o yaml > deploy.yaml
kubectl create -f deploy.yaml
kubectl apply -f deploy.yaml
```
<details><summary>NOTAS</summary>
- Ao criar o deploy são criados pods (po) e o replicaset (rs);
- Pode ser editado o deploy ou rc com kubectl edit.
</details>

Expondo deploy pela porta para acesso a aplicação (cria um service)
```
kubectl expose deploy app1 --dry-run=client --port=8081 --target-port=80 -o yaml  > expose.yaml
kubectl create -f expose.yaml
```

# Aula 1.4 - Criar recursos através do YAML

Aplicar recursos através de um arquivo:
```
kubectl apply -f arquivo.yml
```

Aplicar através de uma pasta:
```
kubectl apply -f pasta/
```

Criar arquivo YAML para um pod:
```
kubectl run nginx --image=nginx --dry-run=client -o yaml
```

Criar arquivo YAML para deploy:
```
kubectl create deploy nginx --image=nginx --dry-run=client -o yaml 
```

# Aula 1.5 - Criar Deploy e Serviço através manifesto YAML

Criar arquivo de deploy:
```
kubectl create deploy nginx --image nginx --dry-run=client -o yaml > deploy.yaml
kubectl apply -f deploy.yaml
kubectl get po,rs,deploy
```

Expor o Deploy:
```
kubectl expose deploy nginx --port=80 --target-port=80 --dry-run=client -o yaml > expose.yaml
kubectl apply -f expose.yaml
kubectl get svc
```

Remover o deploy e o expose:
```
kubectl delete -f deploy.yaml,expose.yaml
```

# LAB Aula 01 - Implementar Aplicação PHP 4Linux




### Implantando uma applicação
Criar o namespace e subir a aplicação
```
kubectl create ns 4labs
kubectl create -f 541/labs/aula01/
```

### Redes

#### Container to Container
Neste exemplo é  criado 1 pod para dois containers com label ops.

Criar o pod
```
kubectl create -f 541/redes/container_to_container.yaml
```
Listar os pods 
```
kubectl get po -l app=ops -o wide
```

Testar a comunicação dentro do node Master.
```
curl IP-Pod1:80
curl IP=Pod1:5000/v2/_catalog
```

#### Pod to Pod
Neste exemplo é criado dois pods e um container em cada pod com label dev.

Criar o pod
```
kubectl create -f 541/redes/pod_to_pod.yaml
```
Listar os pods 
```
kubectl get po -l app=dev -o wide
kubectl get po -l ver=0.5 -o wide
```

Acessar pod1 pelo pod3
```
kubectl exec -it pod3 -- curl IP=Pod1:80
```

#### Pod to Service
Criar o serviço do tipo LoadBalancer para vincular os pod2 e pod3 pela Label app=dev:
```
kubectl create 541/redes/pod_to_service.yaml
```
Listar o serviço:
```
kubectl get svc
```
Para acessar o container, basta adicionar agora o nome nginx-service:
```
kubectl exec -it pod1 -- curl nginx-service
```

## DNS no Kubernetes

Os serviços recebem yn registro DNS do tipo A para um nome no seguinte formato: nome-do-servico.namepspace.svc.cluster.local, um exemplo real disto é o Pod do Kube DNS kube-dns.kube-system.svc.cluster.local
O nome consegue resolver o IP do serviço.

Criar um pod do Busybox através de uma URL do YAML:
```
kubectl create -f https://k8s.io/examples/admin/dns/busybox.yaml
```
Executar um cat para identificar a resolução de nomes do pod.
```
kubectl exec -it busybox -- cat /etc/resolv.conf
```
Podemos ver que o IP do Nameserver é o mesmo do pod kube-dns:
```
kubectl get svc kube-dns -n kube-system
```
Se executar o comando nslookup no Service DNS criado anteriormente, veremos que têm o mesmo IP do kube-dns vinculado:
```
kubectl exec -it busybox -- nslookup nginx-service
```

#### Executar Pod utilizando DNS personalizado.
Criar o pod contendo o Nameserver primário 172.16.1.103 que é o servidor bind da VM kube-registry. A configuração está em /etc/bind/4labs.example
```
kubectl create -f 541/redes/dns-pod.yaml
```
Executar um ping no Pod que acabou de ser criado é bem-sucedido:
```
kubectl exec -it dns-pod -- ping 4labs.example
```
Executar um ping no Pod que já foi criado anteriormente, sem adicionar o DNS personalizado, não será bem-sucedido:
```
kubectl exec -it pod3 -- ping 4labs.example
```

#### Deletando todo o lab até aqui:
```
kubectl delete -f 541/redes/
kubectl delete pod busybox
```

## Configmaps
Um objeto de API que permite armazenar dados como pares de valores-chave. Os pods do Kubernetes podem usar ConfigMaps como arquivos de configuração, variáveis ​​de ambiente ou argumentos de linha de comando como scripts etc.

Criar um configmap chamado variaveis com os seguintes valores literais:
```
kubectl create configmap variaveis --from-literal VAR1=VALOR1 --from-literal VAR2=VALOR2 --from-literal VAR3=VALOR3
```
Listar os configmaps:
```
kubectl get configmaps
```
Para ver os valores adicionados ao configmap:
```
kubectl describe configmap variaveis
```
Para editar este configmap
```
kubectl edit configmap variaveis
```

Criar o configmap com um comando personalizado:
```
kubectl create -f 541/configmaps/bashrc-configmap.yaml
```
Usar o configmap dentro de um Pod. A diferença neste arquivo é a sintaxe envFrom:
```
kubectl create -f 541/configmaps/pod-configmap.yaml
```
Ver a variável que foi criada pelo configmap:
```
kubectl exec pod-configmap -- env  | grep PS1
```
