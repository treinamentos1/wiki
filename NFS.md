1. Criar arquivos $(REPOSITORIO)-pv.yaml e $(REPOSITORIO)-pvc.yaml e preecher os placerholders

**sifbk-backend-pv.yaml**
```
apiVersion: v1
kind: PersistentVolume
metadata:
  annotations:
    pv.kubernetes.io/bound-by-controller: "yes"
  finalizers:
  - kubernetes.io/pv-protection
  name: __REPOSITORIO__-data-__AMBIENTE__
  labels:
    app: sifbk-backend-__AMBIENTE__
spec:
  accessModes:
  - ReadWriteMany
  capacity:
    storage: __SIZE_VOLUME__
  nfs:
    path: __PATH_NFS__
    server: __SERVER_NFS__
  persistentVolumeReclaimPolicy: Retain
```

**sifbk-backend-pvc.yaml**

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  annotations:
  finalizers:
  - kubernetes.io/pvc-protection
  name: __REPOSITORIO__-data-__AMBIENTE__
  namespace: sifbk-__AMBIENTE__
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: __SIZE_VOLUME__
  selector:
    matchLabels:
      app: sifbk-backend-__AMBIENTE__
```

2.  No Azure DevOps Adicionar Library Compartilhamento para usuário e senha do ISILON

3. No Azure DevOps Incluir Taskgroup PERSISTENT_VOLUME_OKD no pipeline

4. Variáveis solicitadas para a WO.

Variáveis | Valor
:-- |:--
__PATH_NFS__  | /export/jboss_eap71_upload_gepem
__SERVER_NFS__ | 10.116.92.160 #OBS: NFS montado em VM
__SIZE_VOLUME__ | 10Gi
__PATH_DESTINO__ | /upload
