Podemos melhorar a interação inserindo o binário do crc para se tornar nativo no nosso sistema operacional.
```shell
sudo cp crc /bin/crc && cp crc /bin/oc
```

Podemos ver o IP da máquina utilizada com Libvirt com o comando `crc ip`.
Podemos fazer login como administrador passando o usuário e senha cedido pela criação do laboratório:
```shell
oc login -u kubeadmin -p wfpQu-HRmfA-een6k-wRZ2g  https://api.crc.testing:6443
```

A intenção é aprender CI/CD e não comandos administrativos do Openshift. Como aministrador, podemos ver o node com o comando `oc get nodes -o wide`

para identificar como fazer o acesso ao console, credenciais e linha de comando, podemos executar `./crc console --credentials`

Para relembrar qual URL do console, podemos executar `crc console --url`

# Projetos
Acesse como administrador. Quando criamos um projeto, criamos um Namespace, para listar Namespaces execute `oc get ns`

## criando projeto por Comandos

Para criar um projeto execute `oc new-project desa1`

Para ver uma descrição do projeto, pode utilizar `oc describe project desa1`

Se desejar exportar a criação do projecto em formato yaml: `oc get project desa1 -o yaml`

Acessando com o usuário administrador a console pelo browser, se formos até **Hoje -> Projects**, veremos que o nosso projeto.

Para um exercício, vamos até **YAML** e vamos editar **openshift.io/display-name: 'Meu primeiro projeto'**. Se formos novamente a linha de comando e executarmos `oc get project desa1`, veremos no Display Name o que acabamos de editar.

## Criando um projeto por arquivo

Para criar um projeto com um arquivo YAML, podemos utilizar este template:
`projeto.yml`
```yaml
apiVersion: project.openshift.io/v1
kind: Project
metadata:
  annotations:
    openshift.io/description: Isto e a descricao do projeto
    openshift.io/display-name: Exemplo de criacao de um projeto Openshift
    openshift.io/requester: developer
    documentacion: Exemplo de criacao de um projeto Openshift
  name: desa2
  labels:
     tipo: desa
spec:
  finalizers:
  - kubernetes
```

Podemos criar o projeto com a linha de comando abaixo:
```shell
oc create -f projeto.yml
```

Veja que adicionamos um **Label tipo: desa**, executarmos então a verificação introduzindo o Label, podemos ver a descrição do projeto filtrando ele.
```shell
oc get project -l tipo=desa
```

Para concluir, se formos até o console web, podemos ver o YAML do projeto.

## Criando projeto pelo console web

Mudando o **usuário para Developer**, podemos ir até **Topology**, no centro, na parte superior veremos o nome **Project**, clicaremos em **Create Project**.
Ao clicar podemos inserir as informações **Name:** desa3, **Display name:** Projeto 3 de aprendizado, **Description:** Descrição do meu projeto.

## Removendo Projeto
Para remover um projeto é tão simples quanto criar. Executamos o seguinte comando:
```shell
oc delete project desa3
```

Podemos também remover um projeto pelo console web clicando nos três pontinhos do lado direito do projeto e selecionando **Delete Project**.
