![](http://blog.4linux.com.br/wp-content/uploads/2017/05/Logotipo.Openshift.png)

## Requisistos
Este processo foi feito no Sistema Operacional Ubuntu 22.04

- 4 physical CPU cores
- 9 GB of free memory
- 35 GB of storage space

## Instalação
Dentro do sistema operacional que está **virtualizando**
```shell
sudo apt install qemu-kvm libvirt-daemon libvirt-daemon-system network-manager
```

Coletar a chave de instalação acessando o site.
https://cloud.redhat.com/openshift/install/crc/installer-provisioned


Baixar o crc
```shell
wget https://developers.redhat.com/content-gateway/file/pub/openshift-v4/clients/crc/2.10.1/crc-linux-amd64.tar.xz
```

Extrair e renomear o diretório
```shell
tar Jxvf crc-linux-amd64.tar.xz && \
mv crc-linux-2.10.1-amd64 crc && cd crc
```

Executamos o setup, Vai demorar um pouco para baixar e extrair.
```shell
./crc config set network-mode user
./crc setup
```

Secret
```bash
{"auths":{"cloud.openshift.com":{"auth":"b3BlbnNoaWZ0LXJlbGVhc2UtZGV2K29jbV9hY2Nlc3NfNGYyMjhmZjc3ZDNhNDA3ZjlhMTAxMWFiZDA5NzE2Yjg6UEtMS0pIR1hBQzU1N04wSkdDRFRRV1JaOVNNQTEySzdEUDc5N0ZGRklFRTdJREEyOElEQzIzMEgySlc4Qk01Ug==","email":"duzerubr@gmail.com"},"quay.io":{"auth":"b3BlbnNoaWZ0LXJlbGVhc2UtZGV2K29jbV9hY2Nlc3NfNGYyMjhmZjc3ZDNhNDA3ZjlhMTAxMWFiZDA5NzE2Yjg6UEtMS0pIR1hBQzU1N04wSkdDRFRRV1JaOVNNQTEySzdEUDc5N0ZGRklFRTdJREEyOElEQzIzMEgySlc4Qk01Ug==","email":"duzerubr@gmail.com"},"registry.connect.redhat.com":{"auth":"fHVoYy1wb29sLTkxZmE5YzA3LTcwZDItNGViYi05Njk2LWUzMTAwNDY1YTk1MzpleUpoYkdjaU9pSlNVelV4TWlKOS5leUp6ZFdJaU9pSmlaVFpoWXpWbFlqYzFOVFkwTnpNM1lqbG1ZekF5WVRZek9XUmtZVGN3T1NKOS5MOVJJNWI1ZXlGbVk4SDNtWUVGRDNJRDhHdjNNWHBlY0YzYVJnX1RGT2FfWEtWYTlsa0lBdG1TX1dMZHA0UW9ReENtYk9FWnQxbjhJR2FuLWppUGJxZUNoZjNpSEg3N1o2TTdjNnM5aFJiVklta2FVVk1CTkg3dk5ZdDhQOFB4RWdyeldqYlJSUFVKbjVEaFNMdURjSWFXRWIzQ1FZanF5bmxpZ2VrUDZXTkpKX3VyMmZqY0FpQ3ZQQXk5SU1mQ0JjR3Jic2NfVzdHMEVaQzVDd0hScTJ1MUtLVjcwb2ZLUGhwT3RkdjZQZ3Y4U1U2cGNnYl9XLTAtOE0yTkFCYzhWcEtTX3VPY0x6LTNqZWlmOTN3ZHNFNHBFVkcxRmJiTW9rOTZMTDFGZExpZzAxZ2JGNjlVYUZ4WDlKZkhHVGg1aDNVOWRlSWRZZTlueUtMcFN0R2lvOEdPRVhLemNHWEhJRGdqQW1GZVJaTUpCeVFwTy15a1Z6eElvalF2dzRKU0Z5eEh6Sk9VNzJNbzNpUEhjR08xZzZHenp6NU5lVi1xdE9qam1ENERSMV9yUEVUa2V3eHZJR3BPMjRoUGZmR3paYWdqcFg5UzQyc19VYnVZVmpYZnk0R2JhdDViTlpqeGNpTVA0TExZVnM0Q3FUWUdSc2dNS2d0eTBoR2NNMUZwTU9MUVk0SGF5MHVESDd6bUdrdUNiay13LUhUM0VYd0FXeDNzbnFkLTBUeWZYbE9RRWFjRkNaTHNPT3ZEYk9YaU5YeTVpdDZzcXVKdEo2bWNjQU4yVDRKZ2hxWE5aLVNHcnJiMlFZZWtwbzZSazVjai1QbFAtdndBRzRtVjloNlhBNTllNS1XMDVaUnFIVlZBeUNiNjRuQ0lxcnVIWHFOeS1iUFJjTTNJZGFGRQ==","email":"duzerubr@gmail.com"},"registry.redhat.io":{"auth":"fHVoYy1wb29sLTkxZmE5YzA3LTcwZDItNGViYi05Njk2LWUzMTAwNDY1YTk1MzpleUpoYkdjaU9pSlNVelV4TWlKOS5leUp6ZFdJaU9pSmlaVFpoWXpWbFlqYzFOVFkwTnpNM1lqbG1ZekF5WVRZek9XUmtZVGN3T1NKOS5MOVJJNWI1ZXlGbVk4SDNtWUVGRDNJRDhHdjNNWHBlY0YzYVJnX1RGT2FfWEtWYTlsa0lBdG1TX1dMZHA0UW9ReENtYk9FWnQxbjhJR2FuLWppUGJxZUNoZjNpSEg3N1o2TTdjNnM5aFJiVklta2FVVk1CTkg3dk5ZdDhQOFB4RWdyeldqYlJSUFVKbjVEaFNMdURjSWFXRWIzQ1FZanF5bmxpZ2VrUDZXTkpKX3VyMmZqY0FpQ3ZQQXk5SU1mQ0JjR3Jic2NfVzdHMEVaQzVDd0hScTJ1MUtLVjcwb2ZLUGhwT3RkdjZQZ3Y4U1U2cGNnYl9XLTAtOE0yTkFCYzhWcEtTX3VPY0x6LTNqZWlmOTN3ZHNFNHBFVkcxRmJiTW9rOTZMTDFGZExpZzAxZ2JGNjlVYUZ4WDlKZkhHVGg1aDNVOWRlSWRZZTlueUtMcFN0R2lvOEdPRVhLemNHWEhJRGdqQW1GZVJaTUpCeVFwTy15a1Z6eElvalF2dzRKU0Z5eEh6Sk9VNzJNbzNpUEhjR08xZzZHenp6NU5lVi1xdE9qam1ENERSMV9yUEVUa2V3eHZJR3BPMjRoUGZmR3paYWdqcFg5UzQyc19VYnVZVmpYZnk0R2JhdDViTlpqeGNpTVA0TExZVnM0Q3FUWUdSc2dNS2d0eTBoR2NNMUZwTU9MUVk0SGF5MHVESDd6bUdrdUNiay13LUhUM0VYd0FXeDNzbnFkLTBUeWZYbE9RRWFjRkNaTHNPT3ZEYk9YaU5YeTVpdDZzcXVKdEo2bWNjQU4yVDRKZ2hxWE5aLVNHcnJiMlFZZWtwbzZSazVjai1QbFAtdndBRzRtVjloNlhBNTllNS1XMDVaUnFIVlZBeUNiNjRuQ0lxcnVIWHFOeS1iUFJjTTNJZGFGRQ==","email":"duzerubr@gmail.com"}}}
```

após concluir a etapa anterior, execute o comando para iniciar o OpenShift:
```shell
./crc start --log-level debug
```

Será apresentado no final do comando, um resumo parecido com este:
```log
Started the OpenShift cluster.

The server is accessible via web console at:
  https://console-openshift-console.apps-crc.testing

Log in as administrator:
  Username: kubeadmin
  Password: gKLHK-EjUbs-cKn22-S955B

Log in as user:
  Username: developer
  Password: developer

Use the 'oc' command line interface:
  $ eval $(crc oc-env)
  $ oc login -u developer https://api.crc.testing:6443
```

Para identificar mais comandos de status, parar, subir entre outros, verifique com o comando:
```shell
./crc --help
```

Caso desejar fazer login por linha de comando e ocorrer algum erro de TLS Handshake, insira a extensão para ignorar o TLS:
```shell
oc login -u developer https://api.crc.testing:6443 --insecure-skip-tls-verify=true
```

Quando você utiliza o **crc**, é criado um diretório no diretório do usuário **~/.crc**, lá contém binários, configurações e a máquina virtual criada com libvirt.

Fonte: https://access.redhat.com/documentation/en-us/red_hat_openshift_local/2.10/html/getting_started_guide/installation_gsg

