![](https://assets.openshift.com/hubfs/Imported_Blog_Media/okd_banner-1024x302.png)

# Introdução

Você está procurando configurar um cluster Openshift e achou incrivelmente difícil?
Talvez no AWS EC2 ou Microsoft Azure VM?

Eu também sou como você, que realmente lutou para configurar o ambiente de laboratório Openshift com custo menor ou sem custo. Ao Eu decidi comprar um Servidor **Tyan**, mas a versão 3.x do RedHat Openshift oferece suporte ao conceito de cluster de nó único e, finalmente, configura um na instância EC2 da AWS. O Kubernetes continua evoluindo e trazendo cada vez mais desenvolvedores para esta robusta plataforma de orquestração. O Openshift 3.11 está chegando ao fim e as organizações estão acelerando para migrar o cluster para o Openshift 4.x. Espero que este artigo o ajude a configurar o playground OKD 3.11 para dar início a sua jornada.

## Pré-requisitos

- Utilizaremos no processo e exemplos de comando para o Lab, um sistema operacional Ubuntu 22.04.
- Teremos o Vitualbox para virtualizar o Minishift
- Teremos o Minishift na versão 1.34.3-linux-amd64

## Instalando Virtualbox
Instale o Virtualbox como desejar.

Após a instalação, se estiver utilizando Linux, precisamos efetuar um ajuste simples para que a rede aceito novos ranges de IP.

`vim /etc/vbox/networks.conf`
```shell
* 0.0.0.0/0 ::/0
```

## Instalando Minishift

Instalando o Openshift e iniciando uma VM com o driver do VirtualBox.

```shell
cd $HOME/Downloads
wget https://github.com/minishift/minishift/releases/download/v1.34.3/minishift-1.34.3-linux-amd64.tgz
tar zxvf minishift-1.34.3-linux-amd64.tgz
mv minishift-1.34.3-linux-amd64.tgz minishift
cd $HOME/Downloads

./minishift config set vm-driver virtualbox
./minishift start vm-driver virtualbox
```

Mesmo que seja uma VM criada no Virtualbox, se encarregue que iniciar e parar esta VM pelo comando do Openshift, pois este comando faz mais que subir a VM.
Ao concluir a execução do comando de start, teremos as informações de acesso do OKD.

Para confirmar a VM e o status, podemos executar o comando do Minishift:
```shell
./minishift status
```

# Introdução

Você está procurando configurar um cluster Openshift e achou incrivelmente difícil?
Talvez no AWS EC2 ou Microsoft Azure VM?

Eu também sou como você, que realmente lutou para configurar o ambiente de laboratório Openshift com custo menor ou sem custo. Ao Eu decidi comprar um Servidor **Tyan**, mas a versão 3.x do RedHat Openshift oferece suporte ao conceito de cluster de nó único e, finalmente, configura um na instância EC2 da AWS. O Kubernetes continua evoluindo e trazendo cada vez mais desenvolvedores para esta robusta plataforma de orquestração. O Openshift 3.11 está chegando ao fim e as organizações estão acelerando para migrar o cluster para o Openshift 4.x. Espero que este artigo o ajude a configurar o playground OKD 3.11 para dar início a sua jornada.

> **DICA**: Se você utilizar Tilix será muito útil para comandos sincronizados no terminal.
{.is-success}


## 1. Pré-requisitos

- Utilizaremos no processo e exemplos de comando para o Lab, um sistema operacional Ubuntu 22.04.
- Teremos o Vitualbox para virtualizar o Minishift
- Teremos o Minishift na versão 1.34.3-linux-amd64

## Virtualbox
Instale o Virtualbox como desejar.

Após a instalação, se estiver utilizando Linux, precisamos efetuar um ajuste simples para que a rede aceito novos ranges de IP.

`vim /etc/vbox/networks.conf`
```shell
* 0.0.0.0/0 ::/0
```

## Openshift

Instalando o Openshift e iniciando uma VM com o driver do VirtualBox.

```shell
cd $HOME/Downloads
wget https://github.com/minishift/minishift/releases/download/v1.34.3/minishift-1.34.3-linux-amd64.tgz
tar zxvf minishift-1.34.3-linux-amd64.tgz
mv minishift-1.34.3-linux-amd64.tgz minishift
cd $HOME/Downloads

./minishift config set vm-driver virtualbox
./minishift start vm-driver virtualbox
```

Mesmo que seja uma VM criada no Virtualbox, se encarregue que iniciar e parar esta VM pelo comando do Openshift, pois este comando faz mais que subir a VM.
Ao concluir a execução do comando de start, teremos no final as informações de acesso do OKD.

Para confirmar a VM e o status, podemos executar o comando do Minishift:
```shell
./minishift status
```

Caso você desejar acessar a VM via SSH
**login:** docker
**pass:** tcuser
**mude pra root:** sudo su
