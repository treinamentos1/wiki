## Alterar a senha do Admin Zabbix 6.x

Acesse o banco de dados zabbixprd com o user de banco zabbix
```
psql -Uzabbix zabbixprd
```

Liste as databases
```
\l
```

Certifique-se que está na database do Zabbix
```
\c zabbixprd
```

```
update users set passwd='$2y$10$92nDno4n0Zm7Ej7Jfsz8WukBfgSS/U0QkIuu8WkJPihXBb2A1UrEK' where username='Admin';
```

Selecione a tabela Users
```
\dt users
```

Execute o comando abaixo para alterar a senha do user Admin para **zabbix**
```
update users set passwd='$2y$10$92nDno4n0Zm7Ej7Jfsz8WukBfgSS/U0QkIuu8WkJPihXBb2A1UrEK' where username='Admin';
```

Saia do banco de dados
```
exit;
````
