```bash
porta 8080

https://github.com/ansible/awx.git

git clone https://github.com/ansible/awx.git

cd awx

make docker-compose-build

make docker-compose

docker exec -ti awx_web awx-manage createsuperuser
```

Fonte:
- https://cloudinfrastructureservices.co.uk/how-to-install-ansible-awx-using-docker-compose-awx-container-20-04/
- https://www.ansiblepilot.com/articles/run-the-latest-ansible-awx-in-docker-containers/
