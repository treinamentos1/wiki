![ansible](https://www.emergent360.com/wp-content/uploads/2022/06/Logo-Red_Hat-Ansible_Automation_Platform-A-Standard-RGB.png)

## Introdução

O Ansible foi centralizado no servidor citdfsrv012 com IP 172.20.0.12 na porta ssh 2223.   
O diretório dos artefatos é **/storage/docker-homol/deploy/ansible-cit/**

#### Instalação e configuração do Ansible
```shell
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```
Deve ser adicionado a configuração da chave SSH no Ansible.
```shell
[defaults]
private_key_file =/storage/docker-homol/deploy/ansible-cit/ansiblecit
```


## Preparação dos servidores gerenciados pelo Ansible



Acessar o Servidor e executar as seguintes etapas:    

1. Criar usuário central e alterar senha
```shell
adduser central
passwd central
```

2. Adicionar o usuário central ao sudo sem pedir senha:
```shell
visudo
central ALL=(ALL)       NOPASSWD:ALL
```

3. Copiar a chave SSH para o servidor:
```shell
ssh-copy-id -i /home/central/.ssh/ansiblecit.pub central@IP-AQUI
```

4. Adicionar o IP ou hostname ao inventário do Ansible.
```shell
/storage/docker-homol/deploy/ansible-cit/inventory
```

5. Executar teste para verificar se o servidor já está gerenciado pelo Ansible com o user *central* e se já tem permissões de elevação do usuário:
```shell
ansible -i inventory IP-AQUI -m shell -a "id"
ansible -i inventory IP-AQUI -b -m shell -a "id"
```
