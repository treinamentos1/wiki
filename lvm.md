Para este exemplo, foi criado um disco de 2G.

# Criando o LVM com primeiro disco

### Criando um PV
Adicionar o disco no Pshical Volume
```
pvcreate /dev/sdb /dev/sdc
```

Listar o Psichal Volume
```
pvs
pvdisplay
```

### Criando o volume group
```
vgcreate vgteste /dev/sdb
```

Listar o Volume Groups
```
vgs
vgdisplay
```

### Criando o Logical Volume com 1.5G do vgteste
```
lvcreate --size 1.5G -n lvteste vgteste
```

Listar o Logical Volume
```
lvs
lvdisplay
```

Caso tiver mais de um LVM no sistema, pode ser listado especificando qual deles
```
lvdisplay /dev/vgteste/lvteste
```

### Adicionado sistema de arquivos, Agora que foi criado o Logical Volume. 
```
mkfs.ext4 /dev/vgteste/lvteste
```

### Montando o Logical Volume
Já podemos montar o volume
```
mount /dev/vgteste/lvteste /mnt
```

Podemos ver o tamanho e tipo de sistema de arquivos com o comando abaixo:
```
df -Th /mnt/
```

# Extendendo o LV para todo o espaço não utilizado do VG.

Para extender não precisa desmontar o volume

```
lvextend -L +1G /dev/docker-vg/docker-lv -r
lvextend -l +100%FREE /dev/vgteste/lvteste
```

Apresentar mudança do LV ao Sistema Operacional temos a flag **-r** ou o comando abaixo:
```
lvextend -L +1G /dev/docker-vg/docker-lv -r
resize2fs /dev/vgteste/lvteste
```

# Adicionando novo disco (Teste feito com um disco de 2G)
```
pvcreate /dev/sdc
vgextend vgteste /dev/sdc
lvextend -l +100%FREE /dev/vgteste/lvteste
resize2fs /dev/vgteste/lvteste
```
Ao efetuar estas ações, caso não tiver montado, forçe a montagem novamente:
```
mount /dev/vgteste/lvteste /mnt
```

# Diminuindo Logical Volume
```
umount /mnt
lvremove /dev/vgteste/lvteste
```
