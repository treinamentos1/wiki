```bash
sudo apt-get install -y openfortivpn
sudo vim /etc/openfortivpn/config
```

```config
# host = 177.159.145.242
# host = 189.112.124.113
# port = 10443
# username = vpnuser
# password = VPNpassw0rd
# trusted-cert =
```

Caso tiver um certificado para utilizar em **trusted-cert**, execute sem sudo para ver os logs e será apresentado um erro parecido com:
`ERROR:      trusted-cert = fb3d7954huah43565lalalalalala42d3c9e576e6c8f5c4d1ba8f7lalalalai9i8372487` 
```bash
sudo openfortivpn
```

Copie este valor **trusted-cert ** para o arquivo e re-execute em segundo plano, caso desejar:
```bash
sudo openfortivpn &
```
