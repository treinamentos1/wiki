```shell
sudo apt-get update && \
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg && \
sudo mkdir -m 0755 -p /etc/apt/keyrings && \
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null && \
sudo apt-get update && \
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

```shell
sudo usermod -aG docker $USER && sudo systemctl enable docker && sudo reboot
```

`sudo vim /etc/hosts`
```shell
192.168.1.110 pg-0
192.168.1.111 pg-1
```

```shell
mkdir -p /storage/postgres-ha/pg-0 && 
sudo chown 1001:1001 /storage/postgres-ha/pg-0
```

```shell
docker run -d --name pg-0 \
  --network host \
  --env REPMGR_PARTNER_NODES=pg-0,pg-1 \
  --env REPMGR_NODE_NAME=pg-0 \
  --env REPMGR_NODE_NETWORK_NAME=pg-0 \
  --env REPMGR_PRIMARY_HOST=pg-0 \
  --env REPMGR_PASSWORD=repmgrpass \
  --env POSTGRESQL_PASSWORD=secretpass \
  -v /storage/postgres-ha/pg-0:/bitnami/postgresql \
  bitnami/postgresql-repmgr:latest
```


mkdir -p /storage/postgres-ha/pg-1 && 
sudo chown 1001:1001 /storage/postgres-ha/pg-1
```shell
docker run -d --name pg-1 \
  --network host \
  --env REPMGR_PARTNER_NODES=pg-0,pg-1 \
  --env REPMGR_NODE_NAME=pg-1 \
  --env REPMGR_NODE_NETWORK_NAME=pg-1 \
  --env REPMGR_PRIMARY_HOST=pg-0 \
  --env REPMGR_PASSWORD=repmgrpass \
  --env POSTGRESQL_PASSWORD=secretpass \
  -v /storage/postgres-ha/pg-1:/bitnami/postgresql \
  bitnami/postgresql-repmgr:latest
```

```shell
docker run -it --rm \
--network host \
bitnami/postgresql:10 \
psql -h pg-0 -U postgres -W
```

```sql
CREATE USER citsmart_usr92 WITH PASSWORD '8tu9GhUuMZRg';
CREATE DATABASE citsmart_db92 WITH OWNER citsmart_usr92 ENCODING 'UTF8' tablespace pg_default;
ALTER DATABASE citsmart_db92 SET timezone to 'America/Fortaleza';
ALTER ROLE citsmart_usr92 SUPERUSER;
CREATE EXTENSION unaccent;
\c citsmart_db92
alter database citsmart_db92 set timezone to 'America/Fortaleza';
CREATE EXTENSION unaccent;
CREATE SCHEMA dbo_audit AUTHORIZATION citsmart_usr92;
show timezone;
```

```sql
docker run -it --rm --network host bitnami/postgresql:10 psql -h 192.168.1.110 -U citsmart_usr92 citsmart_db92 -W
```

```sql
psql -U citsmart_usr92 -d citsmart_db92 -W < citbusiness.dump
```
