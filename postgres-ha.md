### Tecnologias
**O Bitnami** facilita a instalação e execução do seu software de código aberto favorito em qualquer plataforma, incluindo seu laptop, Kubernetes e todas as principais nuvens. Além das ofertas populares da comunidade, a Bitnami, agora parte da VMware, fornece às organizações de TI uma oferta corporativa segura, compatível, mantida continuamente e personalizável para suas políticas organizacionais.

**repmgr** é um conjunto de ferramentas de código aberto para gerenciamento de replicação e failover em um cluster de servidores PostgreSQL. Ele aprimora os recursos de hot standby integrados do PostgreSQL com ferramentas para configurar servidores em standby, monitorar a replicação e executar tarefas administrativas, como failover ou operações manuais de switchover.
repmgr fornece suporte avançado para os mecanismos de replicação integrados do PostgreSQL desde que foram introduzidos na versão 9.0. A série repmgr atual , repmgr 5 , oferece suporte aos últimos desenvolvimentos na funcionalidade de replicação introduzida no PostgreSQL 9.3, como replicação em cascata, alternância de linha do tempo e backups básicos por meio do protocolo de replicação.


### Etapa 1: criar uma rede
```bash
docker network create postgres-ha --driver bridge
```

### Etapa 2: criar a persistência de dados
```bash
mkdir -p /storage/postgres-ha/{pg-0,pg-1,pgadmin} && \
sudo chown -R 1001:1001 /storage/postgres-ha && \
sudo chown 5050:5000 /storage/postgres-ha/pgadmin
```

### Etapa 3: criar uma rede e o nó primário inicial

```bash
docker run -d --name pg-0 \
  --network postgres-ha \
  --env REPMGR_PARTNER_NODES=pg-0,pg-1 \
  --env REPMGR_NODE_NAME=pg-0 \
  --env REPMGR_NODE_NETWORK_NAME=pg-0 \
  --env REPMGR_PRIMARY_HOST=pg-0 \
  --env REPMGR_PASSWORD=repmgrpass \
  --env POSTGRESQL_PASSWORD=secretpass \
  -v /storage/postgres-ha/pg-0:/bitnami/postgresql \
  bitnami/postgresql-repmgr:latest
```

### Etapa 4: criar um nó de espera
```bash
docker run -d --name pg-1 \
  --network postgres-ha \
  --env REPMGR_PARTNER_NODES=pg-0,pg-1 \
  --env REPMGR_NODE_NAME=pg-1 \
  --env REPMGR_NODE_NETWORK_NAME=pg-1 \
  --env REPMGR_PRIMARY_HOST=pg-0 \
  --env REPMGR_PASSWORD=repmgrpass \
  --env POSTGRESQL_PASSWORD=secretpass \
  -v /storage/postgres-ha/pg-1:/bitnami/postgresql \
  bitnami/postgresql-repmgr:latest
```

### Etapa 5: Criando um novo banco de dados no pg-0
```bash
docker run -it --rm \
  --network postgres-ha \
  bitnami/postgresql:10 \
  psql -h pg-0 -U postgres
```


```sql
CREATE USER my4biz01user WITH PASSWORD '123456';
CREATE DATABASE my4biz WITH OWNER my4biz01user ENCODING 'UTF8' tablespace pg_default;
ALTER DATABASE my4biz SET timezone to 'America/Fortaleza';
ALTER ROLE my4biz01user SUPERUSER;
CREATE EXTENSION unaccent;
\c my4biz

CREATE EXTENSION unaccent;
CREATE SCHEMA dbo_audit AUTHORIZATION my4biz01user;
show timezone;
```

Fontes: 
- https://bitnami.com/stack/postgresql-ha/containers
- https://github.com/bitnami/containers/tree/main/bitnami/postgresql-repmgr


### Compose ou Stack
```yaml
version: '3.6'
services:
  pg-0:
    image: docker.io/bitnami/postgresql-repmgr:15
    ports:
      - 5432
    volumes:
      - /storage/postgres-ha/pg-0:/bitnami/postgresql
    environment:
      POSTGRESQL_POSTGRES_PASSWORD: adminpassword
      POSTGRESQL_USERNAME: customuser
      POSTGRESQL_PASSWORD: custompassword
      POSTGRESQL_DATABASE: customdatabase
      REPMGR_PASSWORD: repmgrpassword
      REPMGR_PRIMARY_HOST: pg-0
      REPMGR_PRIMARY_PORT: 5432
      REPMGR_PARTNER_NODES: pg-0,pg-1:5432
      REPMGR_NODE_NAME: pg-0
      REPMGR_NODE_NETWORK_NAME: pg-0
      REPMGR_PORT_NUMBER: 5432
  pg-1:
    image: docker.io/bitnami/postgresql-repmgr:15
    ports:
      - 5432
    volumes:
      - /storage/postgres-ha/pg-1:/bitnami/postgresql
    environment:
      POSTGRESQL_POSTGRES_PASSWORD: adminpassword
      POSTGRESQL_USERNAME: customuser
      POSTGRESQL_PASSWORD: custompassword
      POSTGRESQL_DATABASE: customdatabase
      REPMGR_PASSWORD: repmgrpassword
      REPMGR_PRIMARY_HOST: pg-0
      REPMGR_PRIMARY_PORT: 5432
      REPMGR_PARTNER_NODES: pg-0,pg-1:5432
      REPMGR_NODE_NAME: pg-1
      REPMGR_NODE_NETWORK_NAME: pg-1
      REPMGR_PORT_NUMBER: 5432

  pgadmin:
    container_name: pgadmin
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: "cissa.admin@centralit.com.br"
      PGADMIN_DEFAULT_PASSWORD: "adminpassword"
      PGADMIN_CONFIG_SERVER_MODE: "False"
    volumes:
       - /storage/postgres-ha/pgadmin:/var/lib/pgadmin
    ports:
      - 5050:80
```

Para acessar:
```
docker exec -it ID_CONTAINER bash psql -U postgres
```
senha adminpassword
