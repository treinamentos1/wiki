#### Tenologias
- PostgreSQL 14+238
- Ubuntu 22.04
- Repmgr 5.3.1-1

#### Em todos os servidores

Instale o PostgreSQL: 
```bash
sudo apt update ; sudo apt install -y postgresql=14+238 postgresql-contrib=14+238 wget gnupg2
```

Instale o Repmgr:
```bash
sudo apt update ; sudo apt install -y postgresql-14-repmgr=5.3.1-1
```

Atribua permissão de sudo ao usuário e configure para não solicitar senha:
```bash
sudo tee /etc/sudoers.d/postgres <<EOF
postgres    ALL=(ALL:ALL)   NOPASSWD:ALL
EOF
```

Estas duas linhas abaixo estão especificando regras de acesso para usuários do PostgreSQL que se conectam ao servidor de banco de dados.   
- A primeira linha permite que qualquer usuário com nome de usuário “repmgr” se conecte ao banco de dados “repmgr” a partir de qualquer endereço IP ( 0.0.0.0/0) sem exigir senha (usando o método de autenticação "confiável"). Esta é uma configuração comum para um cluster de replicação PostgreSQL, pois o user repmgr é usado para gerenciar o processo de replicação entre nós no cluster. 
- A segunda linha permite que qualquer usuário “repmgr” se conecte a qualquer banco de dados no servidor com privilégios de replicação (usando a função de banco de dados “replicação”) a partir de qualquer endereço IP ( 0.0.0.0/0) sem exigir senha (usando o método de autenticação "confiável"). Isto é necessário para executar tarefas de replicação, pois precisa ser capaz de se conectar tanto ao banco de dados repmgr e os outros no cluster para monitorar e gerenciar a replicação.
- Obviamente, você não quer permitir que todos se conectem como usuário repmgr, então você precisa alterar 0.0.0.0/0 para sua rede local onde seus nós postgresql escutam, por exemplo 192.168.1.0/24 . 

`sudo vim /etc/postgresql/14/main/pg_hba.conf`
```conf
host    repmgr          repmgr          0.0.0.0/0               trust
host    replication     repmgr          0.0.0.0/0               trust
```


Você também precisa modificar o arquivo postgresql.conf adicione ou modifique as seguintes linhas: 
`sudo vim /etc/postgresql/14/main/postgresql.conf`
```conf
listen_addresses = '*'

listen_addresses = '*'
wal_level = replica
wal_log_hints = on
archive_mode = on
archive_command = 'test ! -f /var/lib/postgresql/14/backup/archivelog/%f && cp %p /var/lib/postgresql/14/backup/archivelog/%f'
restore_command = 'cp /var/lib/postgresql/14/backup/archivelog/%f %p'
archive_cleanup_command = 'pg_archivecleanup /var/lib/postgresql/14/backup/archivelog %r'
max_wal_senders = 10
wal_keep_size = 5000
shared_preload_libraries = 'repmgr'
max_connections = 10000
```

- listen_addresses = '*': especifica as interfaces de rede nas quais o servidor PostgreSQL escutará conexões de entrada. Nesse caso, o servidor está configurado para escutar todas as interfaces de rede disponíveis.
- wal_level = replica: especifica o nível de informações que devem ser gravadas no log write-ahead (WAL) pelo servidor. Nesse caso, o servidor está configurado para gravar informações suficientes para permitir a replicação para outros nós do cluster.
- wal_log_hints = on: especifica se dicas sobre gravações de página inteira devem ser gravadas no WAL. Isso pode melhorar o desempenho da replicação em alguns casos. 
- archive_mode = on: especifica se o arquivamento WAL deve ser habilitado. Quando ativado, o servidor copiará arquivos WAL antigos para um local de arquivo, que pode ser usado para recuperação pontual. 
- archive_command: especifica o comando a ser executado para arquivar um arquivo WAL. Nesse caso, o comando copiará o arquivo WAL para um local de backup, caso ele ainda não exista. 
- restore_command: especifica o comando a ser executado para restaurar um arquivo WAL do arquivo. Neste caso, o comando copiará o arquivo do local de backup para o diretório de dados do PostgreSQL. 
- archive_cleanup_command: especifica o comando a ser executado para remover arquivos WAL arquivados antigos. Neste caso, o comando usará o pg_archivecleanuputilitário para limpar arquivos anteriores ao último ponto de verificação.
- max_wal_senders = 10: especifica o número máximo de conexões simultâneas que podem ser usadas para replicação. 
- wal_keep_segments = 5000: Especifica o número mínimo de segmentos WAL que devem ser mantidos no diretório pg_xlog para garantir que eles estejam disponíveis para replicação. 
- shared_preload_libraries='repmgr': especifica o nome de uma biblioteca compartilhada que deve ser carregada quando o servidor for inicializado. Neste caso, o repmgrestá sendo carregada, o que fornece funcionalidade adicional para gerenciar um cluster de replicação. 
- max_connections = 10000: Indica a quantidade máxima de conexões no banco de dados.

Deve ser criado o diretório de backup para os logs arquivados, também é aconselhável criar um diretório separado para os logs do repmgr para uso posterior. 
```bash
sudo -u postgres mkdir -p /var/lib/postgresql/14/backup/archivelog && \
sudo mkdir /var/log/repmgr; sudo chown postgres:postgres /var/log/repmgr
```

Deve ser criado um usuário e o banco de dados do repmgr, para gerenciar e monitorar a replicação.    
**Portanto, em seu nó primário, execute estes comandos:**
```bash
sudo -u postgres psql

CREATE USER repmgr;
CREATE DATABASE repmgr;
GRANT ALL PRIVILEGES ON DATABASE repmgr TO repmgr;
ALTER USER repmgr WITH SUPERUSER;
ALTER USER repmgr SET search_path TO repmgr, public;
\q
```

Modifique o arquivo abaixo e adicione estas linhas a ele: 
`sudo vim /etc/default/repmgrd`
```conf
REPMGRD_ENABLED=yes
REPMGRD_CONF="/etc/repmgr.conf"
```


Edite o arquivo /etc/repmgr.conf, atente-se a alterar os valores abaixo de acordo com o servidor
- YOUR_HOSTNAME
- YOUR_IP
- NUM_NODE 
`sudo vim /etc/repmgr.conf`
```ini
node_id=1
node_name='node1'
conninfo='host=node1 user=repmgr dbname=repmgr connect_timeout=2'
data_directory='/var/lib/postgresql/14/main'
pg_bindir='/usr/lib/postgresql/14/bin/'
failover=automatic
promote_command='/usr/bin/repmgr standby promote -f /etc/repmgr.conf --log-to-file'
follow_command='/usr/bin/repmgr standby follow -f /etc/repmgr.conf --log-to-file --upstream-node-id=%n'
```

- node_id= NUM_NODE # Numero inteiro unico maior que zero para identificar o node, nao repita este numero em outro server
- node_name='YOUR_HOSTNAME' # IP ou hostname no DNS do servidor
- conninfo='host=YOUR_IP user=repmgr dbname=repmgr connect_timeout=2'
- data_directory='/var/lib/postgresql/14/main' # Raiz do banco de dados
- pg_bindir='/usr/lib/postgresql/14/bin/'
- failover='automatic' # repmgrd tomará a ação se uma situação de failover for detectada
- promote_command='/usr/bin/repmgr standby promote -f /etc/repmgr.conf --log-to-file' #comando que o repmgrd executa ao promover um novo primário
- follow_command='/usr/bin/repmgr standby follow -f /etc/repmgr.conf --log-to-file --upstream-node-id=%n' #comando repmgrd é executado ao instruir um standby a seguir um novo primário - NAO FUNCIONA NA PRATICA



Reinicie os serviços e habilite pra inicializar junto com o sistema operacional:
```bash
sudo systemctl restart postgresql ; sudo systemctl enable postgresql
sudo systemctl start repmgrd ; sudo systemctl enable repmgrd
```

#### Node postgres1 - Primary
Agora que temos todos os pré-requisitos de instalação e serviços postgres e repmgr ativos, **registre o nó primário com o user postgres**.
```bash
sudo -u postgres repmgr primary register -f /etc/repmgr.conf
```

Pode ser visto o status do cluster com o seguinte comando:
```bash
sudo -u postgres repmgr cluster show
```

#### Node postgres2 - Standby
você deve excluir o conteúdo da pasta de dados postgresql para poder replicar o banco de dados primário.    
Altere a palavra PRIMARY_IP para o IP do servidor primário.

```bash
sudo systemctl stop postgresql
#rm -rf /var/lib/postgresql/14/main/*
#su - postgres
sudo -u postgres repmgr -h PRIMARY_IP -U repmgr -d repmgr -f /etc/repmgr.conf standby clone --force --dry-run
# Se tudo der certo no comando de teste acima, continue com o comando abaixo
sudo -u postgres repmgr -h PRIMARY_IP -U repmgr -d repmgr -f /etc/repmgr.conf standby clone
sudo systemctl start postgresql
sudo -u postgres repmgr standby register --force
```

Com estas etapas, o node foi registado no cluster como standby, veja o cluster:
```bash
sudo -u postgres repmgr cluster show
```

#### SPLIT BRAIN - Falha na despromoção de node primary

O Repmgr detecta quando o servidor primary falha e transforma o standby em primary, MAS...
Eu matei à força o processo postmaster no banco de dados primário para fazer o failover. Após o failover, o antigo primário foi interrompido e o outro nó começou a funcionar como primário. Em seguida, reiniciei o antigo primário manualmente e descobri que as funções de ambos os nós eram primárias.

```bash
[postgres@node1 ~]$ repmgr cluster show --verbose
NOTICE: using provided configuration file "/var/lib/pgsql/repmgr.conf"
INFO: connecting to database
 ID | Name                 | Role    | Status    | Upstream | Location | Priority | Timeline | Connection string                       
----+----------------------+---------+-----------+----------+----------+----------+----------+----------------------------------------------------------------------
 1  | node2.postgres-1.com | primary | ! running |          | default  | 100      | 1        | host=192.168.202.228 user=postgres dbname=postgres connect_timeout=2
 2  | node1.postgres-2.com | primary | * running |          | default  | 100      | 2        | host=192.168.202.229 user=postgres dbname=postgres connect_timeout=2

WARNING: following issues were detected
  - node "node2.postgres-1.com" (ID: 1) is running but the repmgr node record is inactive

[postgres@node2 ~]$ repmgr daemon status
 ID | Name                 | Role    | Status               | Upstream | repmgrd | PID  | Paused? | Upstream last seen
----+----------------------+---------+----------------------+----------+---------+------+---------+--------------------
 1  | node2.postgres-1.com | primary | * running            |          | running | 5084 | no      | n/a
 2  | node1.postgres-2.com | standby | ! running as primary |          | running | 2267 | no      | n/a

WARNING: following issues were detected
  - node "node1.postgres-2.com" (ID: 2) is registered as standby but running as primary
```

**Isto é a Síndrome do Cérebro Dividido** ou em inglês (Split Brain). Split Brain é frequentemente usado para descrever o cenário quando dois ou mais nós em um cluster perdem a conectividade entre si, mas continuam a operar independentemente um do outro. 

**Solução:**

Como mencionado acima, vimos um cérebro dividido entre clusters. Ambos os nós funcionam como primários e independentes.

Aqui precisamos que o primário antigo se junte novamente à configuração da réplica e retroceda esse banco de dados para sincronizar a sequência de log do primário atual. Isso descartará quaisquer transações que possam ter chegado ao antigo primário depois que a transição automática ocorreu e foi iniciada.

pare o serviço no primario que falhou, você identificará ao executar o comando `repmgr daemon status`. Será apresentado no status `! running`
```bash
sudo systemctl stop postgresql
```

Refaça o clone do banco de dados, troque OTHER_NODE pelo ip do outro servidor:
```bash
repmgr standby clone -h $OTHER_NODE -U repmgr -d repmgr --force
```

Inicio o serviço postgres
```bash
sudo systemctl stop postgresql
```

Forçe o registro de standby para o node
```bash
repmgr standby register -f /etc/repmgr.conf --force
```

A solução é simples, mas para automatizar, é um pouco mais complexo. Foi criado o script **failover_standby.sh** para fazer isto automaticamente.
insira este script no crontab para se executar. Ele ficará ativo e testará se há problema de split brain a cada 10 segundos. Exemplo do crontab:
`sudo -u postgres crontab -e`
```
* * * * * /bin/failover_standby.sh
```