#!/bin/bash
# Torna o Standby em Primary

OTHER_NODE="" # Node que se tornou primário
LOG_FILE="/var/log/repmgr/repmgrd.log"

# Define o caminho do arquivo de bloqueio, se o arquivo estiver em execução, não execute novamente
LOCK_FILE="/tmp/failover_primary.lock"
# Verifica se o arquivo de bloqueio existe
if [ -e "$LOCK_FILE" ]; then
    echo "Script já está em execução."
    exit 1
fi
# Cria o arquivo de bloqueio
touch "$LOCK_FILE"
# Limpa o arquivo de bloqueio quando o script é interrompido
trap 'rm -f "$LOCK_FILE"' EXIT


# Função para realizar o teste
perform_test() {
    pg_isready -h $OTHER_NODE -p 5432
}

# Loop infinito
while true; do

  # Loop para realizar 10 testes
  for i in {1..20}; do
    echo "Teste $i"
    perform_test

    # Verifica se o status é diferente de 0
    if [ $? -ne 0 ]; then
        echo "Status diferente de 0 detectado. Esperando 60 segundos para verificar novamente..." >> ${LOG_FILE}
        
        sleep 60
        # Verifica novamente após 60 segundos
        perform_test

        # Se o status ainda for diferente de 0 após 60 segundos, promove o standby
        if [ $? -ne 0 ]; then
            echo "Status diferente de 0 persistente. Se este server for standby será promovido para primary..." >> ${LOG_FILE}
            repmgr -f /etc/repmgr.conf standby promote --force
            echo "Despromovido para Standby" >> ${LOG_FILE}
            break
        fi
    fi

    # Espera 1 segundo antes do próximo teste
    sleep 1
  done

done