#!/bin/bash
# Este script torna o server Primary que teve alguma indisponibilidade em Standby
# Importa as variaveis do arquivo de configuracao
source /opt/var.sh


LOG_FILE="$DIR_LOG/failover_standby.log"

# Define o caminho do arquivo de bloqueio
LOCK_FILE="/tmp/failover_standby.lock"
# Função para esperar o serviço PostgreSQL parar
esperar_parar_postgresql() {
  while systemctl is-active --quiet postgresql-13; do
    sleep 1
  done
}

# Função para esperar o serviço PostgreSQL iniciar
esperar_iniciar_postgresql() {
  while ! systemctl is-active --quiet postgresql13; do
    sleep 1
  done
}

# Verifica se o arquivo de bloqueio existe
if [ -e "$LOCK_FILE" ]; then
    echo "Script já está em execução."
    exit 1
fi

# Cria o arquivo de bloqueio
touch "$LOCK_FILE"

# Limpa o arquivo de bloqueio quando o script é interrompido
trap 'rm -f "$LOCK_FILE"' EXIT

# Loop infinito
while true; do

    # Verifica se o serviço PostgreSQL está ativo
    if systemctl is-active --quiet postgresql-13; then # O serviço está ativo
      pg_isready -h $IP_OTHER_NODE -p 5432

        if [ $? -eq 0 ]; then
            echo "["`date "+%Y-%m-%d %H:%M:%S"`"] [INFO] O servico postgresql em ($IP_OTHER_NODE) ESTA ATIVO! inicie o teste de failover." >> ${LOG_FILE}

            # Caso o serviço postgresql deste servidor pare por algum motivo, quando o serviço postgres e repmgrd subirem, aparece a mensagem filtrada no grep
            STATUS_STANDBYERROR=$(repmgr -f /etc/repmgr.conf node check | grep "1 of 1 downstream nodes not attached" | wc -l)
        
            if [ "$STATUS_STANDBYERROR" == "1" ]; then # Se existir a mensagem execute
                #echo "O master caiu mas não virou standby, precisa rebaixar para standby."
                echo "["`date "+%Y-%m-%d %H:%M:%S"`"] [WARN] Este master caiu mas não virou standby, iniciando o processo de de rebaixamento." >> ${LOG_FILE}
                sudo systemctl stop postgresql
                esperar_parar_postgresql # Espera o PostgreSQL parar antes de prosseguir
                sleep 5
                repmgr standby clone -h $IP_OTHER_NODE -U repmgr -d repmgr --force >> "$LOG_FILE" 2>&1 # Redireciona a saída para o arquivo de log
                sudo systemctl start postgresql
                esperar_iniciar_postgresql # Espera o PostgreSQL iniciar antes de prosseguir
                sleep 5
                repmgr standby register -f /etc/repmgr.conf --force >> "$LOG_FILE" 2>&1 # Redireciona a saída para o arquivo de log
                echo "["`date "+%Y-%m-%d %H:%M:%S"`"] [WARN] Processo concluido." >> ${LOG_FILE}
            else # Senão existir a mensagem de erro no node execute
                #echo "PostgreSQL e repmgrd estão OK neste servidor e não há problemas com failover"
                echo "["`date "+%Y-%m-%d %H:%M:%S"`"] [INFO] PostgreSQL e Repmgrd estão OK neste servidor e não há problemas com failover." >> ${LOG_FILE}
            fi
          else
            #echo "O serviço postgresql do outro server NAO ESTA ATIVO, aguardando o servico do outro server subir para iniciar o teste de failover"
            echo "["`date "+%Y-%m-%d %H:%M:%S"`"] [INFO] O postgresql em ($IP_OTHER_NODE) NAO ESTA ATIVO! aguardando o servico do outro servidor subir para testar o failover." >> ${LOG_FILE}
        fi
    else
        #echo "PostgreSQL não está ativo neste servidor. O script vai aguardar"
        echo "["`date "+%Y-%m-%d %H:%M:%S"`"] [INFO] PostgreSQL não está ativo neste servidor. O script vai aguardar o servico subir." >> ${LOG_FILE}
    fi

    sleep 10 # Aguarda 10 segundos antes de executar novamente
done