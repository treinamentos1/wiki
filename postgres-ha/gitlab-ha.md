## Provisionamento

No cenário de VM, vamos ter três maquinas virtuais Ubuntu Server 22.04 Minimal

- RAM: 2G
- CPU: 1N
- HD: 40

Nome | IP | Função
:--|:--|:--
gitlab1 | 192.168.1.201 | Primeira VM do Gitlab
gitlab2 | 192.168.1.202 | Segunda VM para o Gitlab
extras | 192.168.1.203 | NFS, Banco de dados e Redis

```bash
sudo hostnamectl set-hostname gitlab1 && exec bash
sudo hostnamectl set-hostname gitlab2 && exec bash
sudo hostnamectl set-hostname extras && exec bash
```

## Server Extras

### Instalando NFS

Vamos instalar o serviço 
```shell
sudo apt install nfs-kernel-server
```

Criaremos os diretório compartilhados
```shell
sudo mkdir -p \
/storage/gitlab/.ssh \
/storage/gitlab/gitlab-rails/uploads \
/storage/gitlab/gitlab-rails/shared \
/storage/gitlab/gitlab-ci/builds \
/storage/gitlab/git-data
```

Defina permissões para que qualquer usuário na máquina cliente possa acessar a pasta.
```shell
sudo chown -R nobody:nogroup \
/storage/gitlab
```
```shell
sudo chmod -R 777 \
/storage/gitlab
```

Para conceder acesso a clientes NFS, precisaremos definir um arquivo de exportação. O arquivo geralmente está localizado em /etc/exports.
`sudo vim /etc/exports`
```shell
/storage/gitlab/.ssh 192.168.1.201(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-rails/uploads 192.168.1.201(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-rails/shared 192.168.1.201(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-ci/builds 192.168.1.201(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/git-data 192.168.1.201(rw,sync,no_subtree_check,no_root_squash)

/storage/gitlab/.ssh 192.168.1.202(rw,sync,no_subtree_check)
/storage/gitlab/gitlab-rails/uploads 192.168.1.202(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-rails/shared 192.168.1.202(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-ci/builds 192.168.1.202(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/git-data 192.168.1.202(rw,sync,no_subtree_check,no_root_squash)
```

Aplicaremos as configurações, reiniciamos o serviço e habilitamos para iniciar junto ao Sistema Operacional.
```shell
sudo exportfs -av && \
sudo systemctl restart nfs-kernel-server && \
sudo systemctl enable nfs-kernel-server
```

### Instalando Postgres

```shell
sudo apt update && sudo apt install postgresql postgresql-contrib
```

Para manipular o serviço após instalado:
```shell
sudo systemctl stop postgresql
sudo systemctl start postgresql
sudo systemctl status postgresql
sudo systemctl enable postgresql
```

### Criando o Banco de dados
```shell
su postgres
psql -U postgres -c "CREATE DATABASE gitlabhq_production;"
psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE gitlabhq_production TO postgres;"
psql -U postgres -c "ALTER USER postgres PASSWORD 'gitlab';"
```

### Permissão de acesso

Para os demais servidores terem acesso ao Banco de Dados, precisamos incluir esta permissão no arquivo abaixo:
`vim /etc/postgresql/14/main/postgresql.conf`
```shell
listen_addresses = '*'
```

Incluir isto no final do aquivo: 
`vim /etc/postgresql/14/main/pg_hba.conf`
```shell
host all all 0.0.0.0/0 md5
```

Após isto, reinicie o Banco de dados
```shell
sudo systemctl restart postgresql
```

### Instalação do Redis
Siga as etapas descritas abaixo para instalar e configurar o Redis.

Comece executando este comando:
```shell
apt install redis-server
```

vim /etc/redis/redis.conf
```shell
requirepass gitlab
bind 0.0.0.0
```

Por fim, reinicie o serviço Redis executando:
```shell
systemctl restart redis
systemctl enable redis-server
```

Caso tenha acesso a um ambiente Docker e desejar testar a conexão:
```shell
docker run -it --rm redis:4-alpine redis-cli -h 192.168.1.203 -p 6379
```

## Server Gitlab

**Esta etapa deve ser efetuada nos dois nós do Gitlab inicialmente. Após a conclusão bem-sucedida, deve ser feita novamente no Node1**

### Preparar o NFS Client
```shell
sudo apt update && sudo apt install nfs-common -y
```

Crie os diretórios locais que seram o ponto de montagem para o compartilhamento
```shell
sudo mkdir -p /var/opt/gitlab/.ssh \
/var/opt/gitlab/gitlab-rails/uploads \
/var/opt/gitlab/gitlab-rails/shared \
/var/opt/gitlab/gitlab-ci/builds \
/var/opt/gitlab/git-data
```

Caso desejar testar se a montagem está funcionando, pode montar o primeiro diretório manualmente:
```shell
sudo mount -t nfs 192.168.1.203:/storage/gitlab/.ssh /mnt
```

Após confirmar a montagem, desmonte o diretório para montarmos permanentemente posteriormente:
```shell
sudo umount /mnt
```

Para montarmos permanentemente, adicionaremos estas linhas no arquivo fstab:
`sudo vim /etc/fstab`

```shell
192.168.1.203:/storage/gitlab/.ssh /var/opt/gitlab/.ssh nfs defaults 0 0
192.168.1.203:/storage/gitlab/gitlab-rails/uploads /var/opt/gitlab/gitlab-rails/uploads nfs defaults 0 0
192.168.1.203:/storage/gitlab/gitlab-rails/shared /var/opt/gitlab/gitlab-rails/shared nfs defaults 0 0
192.168.1.203:/storage/gitlab/gitlab-ci/builds /var/opt/gitlab/gitlab-ci/builds nfs defaults 0 0
192.168.1.203:/storage/gitlab/git-data /var/opt/gitlab/git-data nfs defaults 0 0
```

Vamos montar todos os diretórios do fstab
```shell
sudo mount -a
```

### Instalar o gitlab

Vamos instalar algumas dependencias do pacote antecipadamente:
```shell
sudo apt-get update && \
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl language-pack-br language-pack-en -y
```

Vamos inserir o repositório e a chave GPG no sistema operacional
```shell
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
```

Instalaremos o gitlab com duas variávels que definiram a URL do serviço e também a senha de root:
```shell
sudo su

EXTERNAL_URL="http://gitlab.devopshero.click"  GITLAB_ROOT_PASSWORD="123456789" apt-get install gitlab-ce=16.9.2-ce.0
```

Após a instalação, você pode efetuar a manipulação do serviço pelos seguintes comandos:

```shell
systemctl stop gitlab-runsvdir
systemctl start gitlab-runsvdir
systemctl status gitlab-runsvdir
systemctl enable gitlab-runsvdir
```

A menos que seja fornecido uma senha personalizada durante a instalação, uma senha será gerada aleatoriamente e armazenada por 24 horas. Use esta senha com nome de usuário root para Entrar.
```shell
cat /etc/gitlab/initial_root_password
```

### Configurando Gitlab

Este processo abrange configurações para o Banco de dados externo e outras ações necessárias para clusterização.

Execute o procedimento de configuração do arquivo gitlab.rb
`vim /etc/gitlab/gitlab.rb`
```yml
external_url 'http://gitlab.devopshero.click'

# Impedir que o GitLab inicie se as montagens de dados NFS nao estiverem disponiveis
high_availability['mountpoint'] = '/var/opt/gitlab/git-data'

# Desabilitar componentes que nao estarao no servidor de aplicativos GitLab
roles ['application_role']
nginx['enable'] = true

# Desabilitar Prometheus e Grafana
prometheus_monitoring['enable'] = false

# PostgreSQL detalhes da conexao
gitlab_rails['db_adapter'] = "postgresql"
gitlab_rails['db_encoding'] = "unicode"
gitlab_rails['db_database'] = "gitlabhq_production"
gitlab_rails['db_username'] = "postgres"
gitlab_rails['db_password'] = "gitlab"
gitlab_rails['db_host'] = "192.168.1.203"
gitlab_rails['db_port'] = 5432

# Redis detalhes da conexao
gitlab_rails['redis_port'] = '6379'
gitlab_rails['redis_host'] = '192.168.1.203'
gitlab_rails['redis_password'] = 'gitlab'
gitlab_rails['redis_ssl'] = false
```

Para impedir que as migrações de banco de dados sejam executadas na atualização. Somente o servidor ou container de aplicativos GitLab principal
```shell
touch /etc/gitlab/skip-auto-migrations
```

Reconfigure o Gitlab para validação dos parâmetros acima:
```shell
gitlab-ctl reconfigure
```

Execute o start no primeiro servidor:
```shell
systemctl start gitlab-runsvdir && \
systemctl enable gitlab-runsvdir && \
systemctl status gitlab-runsvdir
```

Caso sua senha for resetada, utilize o processo abaixo para substituir a senha de root:
```shell
gitlab-rake "gitlab:password:reset[root]"
```

## gitlab2

**SOMENTE** quando concluir a primeira instalação, deve ser efetuada a segunda.
Siga todas as etapas para o novo sistema(Node1).


## Testes

Foi inserido a entra no arquivo abaixo para efetuar o teste:
`vim /etc/hosts`
```
192.168.1.201 gitlab.devopshero.click
192.168.1.202 gitlab.devopshero.click
```

O processo de testes foi feito da seguinte forma nos navegadores:
- Foi acessado em um navegador Firefox o endereço de URL: http://192.168.1.201
- Foi acessado em um navegador Firefox **privadamente** o endereço de URL: http://192.168.1.202
- Foi acessado em um navegador Chrome **privadamente** o endereço de URL: http://gitlab.devopshero.click

### Processos testados

Acessando http://gitlab.devopshero.click
- Criado grupo
- Criado repositório
- Inserido imagens no repositório e grupo
- Feito upload de um arquivo de imagem no repositório
- Feito criação de um arquivo no repositório
- Feito o clone com http
- Desligado um node por vez, como não temos um Load Balace, comentaremos o IP desligado do arquivo `/etc/hosts`.
  - Feito o clone com http
  - Feito upload de um arquivo de imagem no repositório.
  - Feito criação de um arquivo no repositório

# Red Hat

## Server Extras
**Comandos como root**

### Instalando o NFS para compartilhar os dados:
```shell
yum install nfs-utils -y ; systemctl enable nfs-client.target ; systemctl start nfs-client.target ; systemctl status nfs-client.target
```

Adicionando os diretórios do gitlab necessários para a alta disponibilidade:
- **.ssh** Possui chaves autorizadas.
- **gitlab-rails/uploads** Contém anexos do usuário.
- **gitlab-rails/shared** Mantém diretórios de objetos grandes como: Artefatos de CI, diffs de solicitação de mesclagem externa, repositório de pacotes, páginas do usuário e outros.
- **gitlab-ci/builds** Mantém logs de compilação de CI.
- **git-data** Mantém o diretório de repositórios.

Criaremos os diretório compartilhados
```shell
mkdir -p \
/storage/gitlab/git-data \
/storage/gitlab/gitlab-ci/builds \
/storage/gitlab/gitlab-rails/shared \
/storage/gitlab/gitlab-rails/uploads \
/storage/gitlab/.ssh
```

Defina permissões para que qualquer usuário na máquina cliente possa acessar a pasta.
```shell
chown -R nobody:nobody \
/storage/gitlab/.ssh \
/storage/gitlab/gitlab-rails \
/storage/gitlab/gitlab-ci \
/storage/gitlab/git-data
```
```shell
chmod -R 777 /storage/gitlab/.* && \
chmod -R 777 /storage/gitlab
```

Deve ser inserido as seguintes configurações no arquivo de compartilhamento NFS.
`vim /etc/exports`
```shell
/storage/gitlab/.ssh 10.116.78.77(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-rails/uploads 10.116.78.77(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-rails/shared 10.116.78.77(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-ci/builds 10.116.78.77(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/git-data 10.116.78.77(rw,sync,no_subtree_check,no_root_squash)

/storage/gitlab/.ssh 10.116.78.78(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-rails/uploads 10.116.78.78(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-rails/shared 10.116.78.78(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/gitlab-ci/builds 10.116.78.78(rw,sync,no_subtree_check,no_root_squash)
/storage/gitlab/git-data 10.116.78.78(rw,sync,no_subtree_check,no_root_squash)
```

Deve ser aplicado e reiniciado o serviço NFS:
```shell
exportfs -arv && \
systemctl restart nfs-server ; systemctl enable nfs-server ; systemctl status nfs-server
``` 

### Banco de dados Postgres

O a versão 13, no momento desta implantação é a mais recente. Vamos instalar este módulo:
```shell
dnf module install postgresql:13
```

Instalando o Postgresql
```shell
dnf install -y postgresql postgresql-contrib
```

Iniciar o serviço, habilitar para Inicializar junto com sistema Operacional e ver o status:
```shell
postgresql-setup --initd && \
systemctl start postgresql && \
systemctl enable postgresql && \
systemctl status postgresql
```

Criando o Banco de dados acessando com usuário **postgres**
```shell
su postgres
cd ~/
psql -U postgres -c "CREATE DATABASE gitlabhq_production;"
psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE gitlabhq_production TO postgres;"
psql -U postgres -c "ALTER USER postgres PASSWORD 'gitlab';"
exit
```

Permissão de acesso para os demais servidores terem acesso ao Banco de Dados, precisamos incluir esta permissão no arquivo abaixo:
`/var/lib/pgsql/data/postgresql.conf`
```conf
listen_addresses = '*'
```

Incluir isto no final do aquivo:
`/var/lib/pgsql/data/pg_hba.conf`
```conf
host    all             all             0.0.0.0/0               md5
```

Após isto, reinicie o Banco de dados, saia do usuário postgres para o usuário root.
```shell
systemctl restart postgresql
```

### Instalando e configurando o Redis
Siga as etapas descritas abaixo para instalar e configurar o Redis.

Instalar o Redis
```shell
dnf install -y redis
```

Configurar para receber conexões:
`vim /etc/redis.conf`
```shell
bind 0.0.0.0
```

Reiniciar o Redis habilitar e habilitar a inicialização:
```shell
systemctl restart redis ; systemctl enable redis ; systemctl status redis
```

### habilitando portas:

Postgresql
```shell
firewall-cmd --zone="public" --add-port=5432/tcp --permanent
```
Redis
```shell
firewall-cmd --zone="public" --add-port=6379/tcp --permanent
```
NFS
```shell
firewall-cmd --zone="public" --add-port=2048/tcp --permanent && \
firewall-cmd --zone="public" --add-port=2049/tcp --permanent && \
firewall-cmd --zone="public" --add-port=111/tcp --permanent
```

Reiniciar o serviço
```shell
firewall-cmd --reload
```

## Server Gitlab

### Preparar o NFS Client
```shell
yum install nfs-utils -y ; systemctl enable nfs-client.target ; systemctl start nfs-client.target
```

Crie os diretórios locais que seram o ponto de montagem para o compartilhamento
```shell
mkdir -p /var/opt/gitlab/.ssh && \
mkdir -p /var/opt/gitlab/gitlab-rails/uploads && \
mkdir -p /var/opt/gitlab/gitlab-rails/shared && \
mkdir -p /var/opt/gitlab/gitlab-ci/builds && \
mkdir -p /var/opt/gitlab/git-data
```

Caso desejar testar se a montagem está funcionando, pode montar o primeiro diretório manualmente:
```shell
mount -t nfs 10.116.78.79:/storage/gitlab/.ssh /var/opt/gitlab/.ssh
```

Após confirmar a montagem, desmonte o diretório para montarmos permanentemente posteriormente:
```shell
umount /var/opt/gitlab/.ssh
```

Para montarmos permanentemente, adicionaremos estas linhas no arquivo fstab:
`vim /etc/fstab`
```conf
## Gitlab
10.116.78.79:/storage/gitlab/.ssh /var/opt/gitlab/.ssh nfs defaults 0 0
10.116.78.79:/storage/gitlab/gitlab-rails/uploads /var/opt/gitlab/gitlab-rails/uploads nfs defaults 0 0
10.116.78.79:/storage/gitlab/gitlab-rails/shared /var/opt/gitlab/gitlab-rails/shared nfs defaults 0 0
10.116.78.79:/storage/gitlab/gitlab-ci/builds /var/opt/gitlab/gitlab-ci/builds nfs defaults 0 0
10.116.78.79:/storage/gitlab/git-data /var/opt/gitlab/git-data nfs defaults 0 0
```

Montando e checando todos os diretórios apresentados no arquivo fstab
```shell
mount -a ; df -h
```

### Instalar o gitlab

instalar algumas dependencias do pacote antecipadamente:
```shell
dnf install -y curl policycoreutils openssh-server perl
```

Caso o servidor não tiver internet, deve ser selecionado na URL abaixo a versão e enviado o arquivo para o servidor:
```shell
https://packages.gitlab.com/gitlab
```

Foi selecionado abaixo a versão Community:
```shell
rpm -ivh gitlab-ce-14.0.12-ce.0.el8.x86_64.rpm
```

### Configurando o Gitlab

Este processo abrange configurações para o Banco de dados externo e outras ações necessárias para clusterização.

Execute o procedimento de configuração do arquivo gitlab.rb
`vim /etc/gitlab/gitlab.rb`
```rb
# Caso houver problema no usuário do git
#user['username'] = "gitlab-www"
#user['group'] = "gitlab-www"

# A URL que o Gitlab será chamado
external_url 'http://gitlab-ha.caixa.gov.br'

# Impedir que o GitLab inicie se as montagens de dados NFS nao estiverem disponiveis
high_availability['mountpoint'] = '/var/opt/gitlab/git-data'

# Desabilitar componentes que nao estarao no servidor de aplicativos GitLab
roles ['application_role']
nginx['enable'] = true

# Desabilitar Prometheus e Grafana
prometheus_monitoring['enable'] = false

# PostgreSQL detalhes da conexao
gitlab_rails['db_adapter'] = "postgresql"
gitlab_rails['db_encoding'] = "unicode"
gitlab_rails['db_database'] = "gitlabhq_production"
gitlab_rails['db_username'] = "postgres"
gitlab_rails['db_password'] = "gitlab"
gitlab_rails['db_host'] = "10.116.78.79"
gitlab_rails['db_port'] = 5432

# Redis detalhes da conexao
gitlab_rails['redis_port'] = '6379'
gitlab_rails['redis_host'] = '10.116.78.79'
gitlab_rails['redis_ssl'] = false

gitlab_rails['redis_password'] = 'gitlab'

## Disable automatic database migrations
## Only the primary GitLab application server should handle migrations
gitlab_rails['auto_migrate'] = false
```

Deve ser atribuido a permissão 777 no diretório de chaves de ssh compartilhadas entre o cluster.
```shell
chmod -R 777 /var/opt/gitlab/.ssh
```

Execute a configuração do gitlab
```shell
systemctl start gitlab-runsvdir
gitlab-ctl reconfigure
```

> Há a possibilidade de travar o processo por algum motivo particular do Sistema Operacional Red Hat, neste caso, é necessário matar o processo com <kbd>CTRL</kbd>+<kbd>C</kbd> e reiniciando o servidor. 

Após o reinício do Sistema Operacional, acessar como **root** e executar os seguintes comandos:
```shell
systemctl start gitlab-runsvdir ; systemctl enable gitlab-runsvdir ; systemctl status gitlab-runsvdir
usermod -s /bin/false -d /var/opt/gitlab/nginx gitlab-www
gitlab-ctl reconfigure
```

### Permissões de Firewall ao gitlab
```shell
firewall-cmd --permanent --add-service=http && \
sudo firewall-cmd --permanent --add-service=https && \
sudo systemctl reload firewalld
```

### Acessando o Gitlab

É importantíssimo efetuar o primeiro acesso. Uma senha será gerada aleatoriamente e armazenada por 24 horas. Use esta senha com nome de usuário **root** para o acesso.
```shell
cat /etc/gitlab/initial_root_password
```

Se por algum motivo houver falha de acesso com a senha, utilize o processo abaixo para substituir a senha de root:
```shell
gitlab-rake "gitlab:password:reset[root]"
```

Para impedir que as migrações de banco de dados sejam executadas nos novos nodes. Execute este comando no servidor.
```shell
touch /etc/gitlab/skip-auto-migrations
```

## Node1

**SOMENTE quando concluir a primeira instalação e todos os outros passos, deve ser efetuada a segunda.**
Siga todas as etapas de instalação do Gitlab segundo servidor para a Alta Disponibilidade.


# Comandos úteis

## Logs do Gitlab
```shell
gitlab-ctl tail
```

## Status dos serviços Gitlab
```shell
gitlab-ctl status
```

## Comandos úteis
gitlab-ctl tail - Log
gitlab-rake gitlab:env:info - Informações
gitlab-rake gitlab:check - Checar configurações


# Portas utilizadas pelo Omnibus
https://docs.gitlab.com/ee/administration/package_information/defaults.html 

# Fontes:
- http://repositories.compbio.cs.cmu.edu/help/administration/high_availability/gitlab.md
- https://stackoverflow.com/questions/70139731/docker-compose-yml-with-gitlab-omnibus-config-not-working
- https://docs.gitlab.com/ee/security/reset_user_password.html
- https://docs.gitlab.com/ee/administration/postgresql/external.html
- https://docs.gitlab.com/ee/install/requirements.html#database
- https://docs.gitlab.com/ee/administration/uploads.html
- https://repository.prace-ri.eu/git/help/administration/uploads.md#:~:text=The%20uploads%20are%20stored%20by,%2Fgitlab%2Drails%2Fuploads%20.&text=This%20setting%20only%20applies%20if,gitlab_rails%5B'uploads_storage_path'%5D%20directory
- https://about.gitlab.com/install/#ubuntu
- https://redis.io/docs/getting-started/installation/install-redis-on-linux/
- https://www.ibm.com/docs/pt-br/api-connect/5.0.x?topic=profiles-generating-self-signed-certificate-using-openssl
- https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart-pt
