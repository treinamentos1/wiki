## Criando usuário
```bash
adduser deploy && usermod -aG sudo deploy
echo "deploy ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/deploy
su deploy
```

## Instalando ferramentas

#### node

```bash
sudo apt update && sudo apt install curl git -y
curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash -
sudo apt update && apt install nodejs -y
```

#### Docker
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
sudo apt install docker-ce -y
sudo systemctl enable docker
sudo usermod -aG docker ${USER}
```

#### Criando banco de dados

```bash
docker run --name whaticketdb -e MYSQL_ROOT_PASSWORD=strongpassword -e MYSQL_DATABASE=whaticket -e MYSQL_USER=whaticket -e MYSQL_PASSWORD=whaticket --restart always -p 3306:3306 -d mariadb:latest --character-set-server=utf8mb4 --collation-server=utf8mb4_bin
```

## Instalação

Baixe o conteúdo
```bash
cd ~
git clone https://github.com/canove/whaticket-community.git
cd whaticket-community/backend/
```

#### Instalar dependencias do puppeteer para algo desconhecido mas está nja documentação

```bash
sudo apt-get install -y libxshmfence-dev libgbm-dev wget unzip fontconfig locales gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils
```

#### Arquivo .env

**para localhost**
vim .env
```conf
NODE_ENV=
BACKEND_URL=http://localhost
FRONTEND_URL=http://localhost:3000
PROXY_PORT=8080
PORT=8080

DB_DIALECT=mysql
DB_HOST=localhost
DB_USER=root
DB_PASS=strongpassword
DB_NAME=whaticket

JWT_SECRET=3123123213123
JWT_REFRESH_SECRET=7575675675
```

**Para Cloudflare**
```conf
NODE_ENV=
BACKEND_URL=https://chatapi.prazeresigilo.com
FRONTEND_URL=https://chat.prazeresigilo.com
PROXY_PORT=443
PORT=8080

DB_DIALECT=mysql
DB_HOST=localhost
DB_USER=root
DB_PASS=strongpassword
DB_NAME=whaticket

JWT_SECRET=3123123213123
JWT_REFRESH_SECRET=7575675675
```

Instalar dependências de backend, criar aplicativo, executar migrações e sementes:
```bash
npm install
npm run build
npx sequelize db:migrate
npx sequelize db:seed:all
```

## Nginx

```bash
sudo apt install nginx
```

`vim /etc/nginx/conf.d/chatapi.conf`
```bash
# Configuração para chatapi.prazeresigilo.com redirecionando para a porta 8080 internamente
underscores_in_headers on;

server {
    listen 80;
    server_name chatapi.prazeresigilo.com;

    location / {
        #Correcao bug de refresh nas msgs
        proxy_cache_bypass $http_upgrade;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        #
        proxy_pass http://localhost:8080;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

Teste
```bash
npm start
```

Para execução direta, pare o comando acima com CTRL+C, Instale o pm2 com sudo e inicie o backend com ele:
```bash
sudo npm install -g pm2
pm2 start dist/server.js --name whaticket-backend

# Faça o pm2 iniciar automaticamente após a reinicialização:
pm2 startup ubuntu -u deploy
sudo env PATH=$PATH:/usr/bin pm2 startup ubuntu -u deploy --hp /home/deploy
```


## Frontend
Vá para a pasta frontend e instale as dependências:
```bash
cd ../frontend
npm install
```

Crie um arquivo .env do frontend e preencha-o SOMENTE com seu endereço de backend. Ele deve ficar assim:
```conf
REACT_APP_BACKEND_URL = https://chatapi.prazeresigilo.com
```

Crie um aplicativo frontend:
```bash
export NODE_OPTIONS=--openssl-legacy-provider
npm run build
```

para teste
```bash
npm start
```

#### Nginx

`vim /etc/nginx/conf.d/chatapi.conf`
```conf
# Configuração para chat.prazeresigilo.com redirecionando para a porta 3000 internamente
server {
    listen 80;
    server_name chat.prazeresigilo.com;

    location / {
        proxy_pass http://localhost:3000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

Para execução direta, pare o serviço com CTRL+C, Inicie o frontend com pm2 e salve a lista de processos pm2 para iniciar automaticamente após a reinicialização:
```bash
pm2 start server.js --name whaticket-frontend
pm2 save
```

**NOTA** Aguarde uns 2 minutos pois o FrontEnd demora subir um pouco.

Usuário: admin@whaticket.com Senha: admin

para ver logs:
```bash
pm2 logs
```

#### Variavel
`vim /home/deploy/.bashrc`
```bash
export NODE_OPTIONS=--openssl-legacy-provider
```

```bash
source /home/deploy/.bashrc
```


#### BUG

Encontrei um bug no frontend, quando executava o comando `pm2 start server.js --name whaticket-frontend` não abria a porta 3000 nem rodava a aplicação.

Para contornar, executei uma dica do chatbot
```bash
pm2 start npm --name "whaticket" -- start
```

ele também mostrou outra opção:
```bash
npm install -g forever
forever start -l /home/deploy/whaticket-community/frontend/logs.log -o /home/deploy/whaticket-community/frontend/logs.log -e /home/deploy/whaticket-community/frontend/logs.log /home/deploy/whaticket-community/frontend/node_modules/.bin/react-scripts start
```